const { Engine, Render, World, Bodies, Mouse, MouseConstraint, Runner } = Matter;

// Déclaration de appState
const appState = {
    spacePressed: false,
    // autres variables...
};

// Configuration du canevas
const canvas = document.getElementById('canvas');
const canvasWidth = 800;
const canvasHeight = 600;
canvas.width = canvasWidth;
canvas.height = canvasHeight;

// Création de l'engin physique
const engine = Engine.create();
const render = Render.create({
    element: document.body,
    engine: engine,
    canvas: canvas,
    options: {
        width: canvas.width,
        height: canvas.height,
        wireframes: false
    }
});

// Démarrer le rendu
Render.run(render);
Runner.run(Runner.create(), engine);

// Variables pour stocker les bulles et les joueurs
let bubbles = [];
let players = {};
let selectedBubbles = [];
let gravityEnabled = false;

// Définir les diamètres minimum et maximum
const MIN_RADIUS = 10; // Rayon minimum
const MAX_RADIUS = 150; // Rayon maximum

// Fonction utilitaire pour générer des valeurs aléatoires
const randomValue = (min, max) => Math.random() * (max - min) + min;

// Fonction pour générer une couleur aléatoire
const randomColor = () => `hsl(${Math.random() * 360}, 100%, 50%)`;

// Fonction pour créer une bulle
const createBubble = (position) => {
    const radius = Math.max(MIN_RADIUS, Math.min(randomValue(10, 40), MAX_RADIUS)); // Limiter le rayon
    const fillColor = randomColor(); // Générer une couleur aléatoire
    const bubble = Bodies.circle(position.x, position.y, radius, {
        restitution: randomValue(0, 1),
        friction: randomValue(0, 1),
        density: randomValue(0.1, 1),
        airFriction: randomValue(0.0001, 1),
        maxSpeed: randomValue(0, 100),
        render: { fillStyle: fillColor } // Utiliser la couleur générée
    });
    bubble.properties = { radius, originalColor: fillColor, fillStyle: fillColor, strokeStyle: 'transparent' }; // Assurez-vous que fillStyle est défini ici
    return bubble;
};

// Fonction pour ajouter une bulle
function addBubble() {
    const position = { x: Math.random() * canvasWidth, y: Math.random() * canvasHeight };
    const bubble = createBubble(position);
    World.add(engine.world, bubble);
    bubbles.push(bubble);
    updateDashboard();
    drawBubbles();
}

// Fonction pour dessiner les bulles
function drawBubbles() {
    const bubbleSelection = d3.select("#canvas").selectAll("circle").data(bubbles, d => d.id);
    
    // Créer les cercles pour les bulles non sélectionnées
    bubbleSelection.enter()
        .append("circle")
        .attr("class", "bubble")
        .attr("cx", d => d.position.x)
        .attr("cy", d => d.position.y)
        .attr("r", d => d.properties.radius)
        .style("fill", d => d.properties.originalColor) // Couleur de remplissage
        .style("stroke", "transparent") // Pas de contour par défaut
        .style("stroke-width", 0);

    // Mettre à jour les bulles existantes
    bubbleSelection
        .attr("cx", d => d.position.x)
        .attr("cy", d => d.position.y)
        .attr("r", d => d.properties.radius)
        .style("fill", d => d.properties.originalColor);

    // Dessiner les bulles sélectionnées après les autres
    const selectedBubblesSelection = bubbleSelection.filter(d => selectedBubbles.includes(d));
    
    // Créer les cercles pour les bulles sélectionnées
    selectedBubblesSelection.enter()
        .append("circle")
        .attr("class", "bubble selected") // Ajouter une classe pour les bulles sélectionnées
        .attr("cx", d => d.position.x)
        .attr("cy", d => d.position.y)
        .attr("r", d => d.properties.radius)
        .style("fill", d => d.properties.originalColor) // Couleur de remplissage
        .style("stroke", "rgba(255, 255, 255, 0.7)") // Appliquer le contour
        .style("stroke-width", 4); // Épaisseur du contour

    // Mettre à jour les bulles sélectionnées
    selectedBubblesSelection
        .attr("cx", d => d.position.x)
        .attr("cy", d => d.position.y)
        .attr("r", d => d.properties.radius)
        .style("fill", d => d.properties.originalColor) // Couleur de remplissage
        .style("stroke", "rgba(255, 255, 255, 0.7)") // Appliquer le contour
        .style("stroke-width", 4); // Épaisseur du contour

    // Supprimer les bulles qui ne sont plus présentes
    bubbleSelection.exit().remove();
}

// Fonction pour mettre à jour le tableau de bord
function updateDashboard() {
    d3.select("#dashboard").selectAll("div.bubble-info").remove();
    bubbles.forEach((bubble, index) => {
        const playerList = players[bubble.id] || [];
        const playerNames = playerList.map(player => `${player.name} (Niveau: ${player.level})`).join(", ");
        const bubbleInfo = d3.select("#dashboard").append("div").attr("class", "bubble-info").text(`Bulle ${index + 1}: ${playerNames}`);
        playerList.forEach(player => {
            bubbleInfo.append("button")
                .text(`Supprimer ${player.name}`)
                .on("click", () => removePlayerFromBubble(bubble.id, player.name));
        });
        bubbleInfo.append("input")
            .attr("type", "number")
            .attr("placeholder", "Nouveau niveau")
            .on("change", function() {
                const newLevel = this.value;
                playerList.forEach(player => player.level = newLevel);
                updateDashboard();
            });
    });
    const averageLevel = calculateAverageLevel();
    d3.select("#dashboard").append("div").attr("class", "average-level").text(`Niveau moyen des joueurs : ${averageLevel}`);
}

// Fonction pour calculer le niveau moyen des joueurs
function calculateAverageLevel() {
    const allLevels = Object.values(players).flat().map(player => player.level);
    const total = allLevels.reduce((sum, level) => sum + parseInt(level, 10), 0);
    return allLevels.length > 0 ? (total / allLevels.length).toFixed(2) : 0;
}

// Fonction pour dessiner les joueurs dans les bulles
function drawPlayers() {
    World.clear(engine.world, false);
    bubbles.forEach(bubble => {
        const playerList = players[bubble.id] || [];
        playerList.forEach((player, index) => {
            const playerBody = Bodies.circle(bubble.position.x + (index * 10), bubble.position.y, player.level * 2, {
                isStatic: true,
                render: { fillStyle: 'black' }
            });
            World.add(engine.world, playerBody);
        });
    });
}

// Gestion des interactions avec la souris
const mouse = Mouse.create(render.canvas);
const mouseConstraint = MouseConstraint.create(engine, { mouse, constraint: { stiffness: 0.2, render: { visible: false } } });
World.add(engine.world, mouseConstraint);
render.mouse = mouse;

// Événements de survol de la souris
let originalBubbleSize = null;
Matter.Events.on(mouseConstraint, 'mouseover', function(event) {
    const bubble = mouseConstraint.body;
    if (bubble && bubble.render) {
        originalBubbleSize = bubble.properties.radius; // Stocker la taille d'origine
        const scaleFactor = 1.33; // 33% d'augmentation
        const newRadius = Math.min(originalBubbleSize * scaleFactor, MAX_RADIUS); // Limiter à MAX_RADIUS
        Matter.Body.scale(bubble, newRadius / originalBubbleSize, newRadius / originalBubbleSize);
        bubble.properties.radius = newRadius; // Mettre à jour le rayon
    }
});

// Événements de fin de survol de la souris
Matter.Events.on(mouseConstraint, 'mouseout', function(event) {
    const bubble = mouseConstraint.body;
    if (bubble && bubble.render) {
        Matter.Body.scale(bubble, originalBubbleSize / bubble.properties.radius, originalBubbleSize / bubble.properties.radius);
        bubble.properties.radius = originalBubbleSize; // Rétablir le rayon d'origine
    }
});

// Événements de dézoom/zoom
document.addEventListener('wheel', function(event) {
    const bubble = mouseConstraint.body;
    if (bubble) {
        event.preventDefault(); // Empêcher le défilement de la page
        const scaleFactor = event.deltaY < 0 ? 1.05 : 0.95; // Réduire encore le pas de zoom
        const newRadius = Math.max(MIN_RADIUS, Math.min(bubble.properties.radius * scaleFactor, MAX_RADIUS)); // Limiter à MIN_RADIUS et MAX_RADIUS
        Matter.Body.scale(bubble, newRadius / bubble.properties.radius, newRadius / bubble.properties.radius);
        bubble.properties.radius = newRadius; // Mettre à jour le rayon
    }
});

// Événements de glissement
Matter.Events.on(mouseConstraint, 'startdrag', function(event) {
    const bubble = event.body;
    if (bubble) {
        toggleBubbleSelection(bubble);
    }
});

// Événements de glissement continu
Matter.Events.on(mouseConstraint, 'enddrag', function(event) {
    const bubble = event.body;
    if (bubble) {
        // Logique à exécuter lorsque le glissement se termine, si nécessaire
    }
});

// Mise à jour de la position de la bulle pendant le glissement
Matter.Events.on(mouseConstraint, 'mousemove', function(event) {
    const bubble = mouseConstraint.body;
    if (bubble) {
        // Mettre à jour la position de la bulle avec la position de la souris
        Matter.Body.setPosition(bubble, { x: event.mouse.position.x, y: event.mouse.position.y });
    }
});

// Fonction utilitaire pour gérer la sélection des bulles
function toggleBubbleSelection(bubble) {
    if (!selectedBubbles.includes(bubble)) {
        selectedBubbles.push(bubble);
        bubble.render.strokeStyle = bubble.properties.fillStyle; // Couleur de sélection égale à la couleur de remplissage
        bubble.render.lineWidth = 4; // Épaisseur de sélection
        animateBubbleStroke(bubble); // Démarrer l'animation de pulsation
    } else {
        selectedBubbles = selectedBubbles.filter(b => b !== bubble);
        // Restaurer les propriétés de contour d'origine
        bubble.render.strokeStyle = bubble.properties.strokeStyle || 'transparent'; // Couleur d'origine
        bubble.render.lineWidth = bubble.properties.lineWidth || 0; // Épaisseur d'origine
    }
    updateDashboard(); // Mettre à jour le tableau de bord après la sélection
    drawBubbles(); // Redessiner les bulles pour appliquer les changements
}

// Événements de collision
Matter.Events.on(engine, 'collisionStart', function(event) {
    // Logique de collision si nécessaire
});

// Fonction pour activer/désactiver la gravité centrale
function toggleCentralGravity() {
    gravityEnabled = !gravityEnabled;
}

// Événements de clavier
document.addEventListener('keydown', function(event) {
    if (event.code === 'Space') {
        appState.spacePressed = true; // La touche espace est enfoncée
    }

    // Vérifier si la touche espace est maintenue
    if (appState.spacePressed) {
        if (event.key === 'Delete' && bubbles.length > 0) {
            const bubbleToRemove = bubbles.pop();
            World.remove(engine.world, bubbleToRemove);
            updateDashboard();
        } else if (event.key === 'r') {
            bubbles.forEach(bubble => World.remove(engine.world, bubble));
            bubbles = [];
            players = {};
            updateDashboard();
        } else if (event.key === 'g') {
            toggleCentralGravity();
        } else if (event.key === 's') {
            saveState();
        } else if (event.key === 'l') {
            loadState();
        }
    }
});

// Écouter l'événement de relâchement de la touche
document.addEventListener('keyup', function(event) {
    if (event.code === 'Space') {
        appState.spacePressed = false; // La touche espace est relâchée
    }
});

// Mise à jour de la gravité centrale à chaque tick
Matter.Events.on(engine, 'beforeUpdate', function() {
    if (gravityEnabled) {
        const centerX = canvasWidth / 2;
        const centerY = canvasHeight / 2;
        bubbles.forEach(bubble => {
            const dx = centerX - bubble.position.x;
            const dy = centerY - bubble.position.y;
            const distance = Math.sqrt(dx * dx + dy * dy);
            const forceMagnitude = 0.005 * bubble.mass;
            Matter.Body.applyForce(bubble, bubble.position, { x: (dx / distance) * forceMagnitude, y: (dy / distance) * forceMagnitude });
        });
    }
    bubbles.forEach(bubble => {
        const maxSpeed = bubble.properties.maxSpeed;
        const speed = Math.sqrt(bubble.velocity.x ** 2 + bubble.velocity.y ** 2);
        if (maxSpeed > 0 && speed > maxSpeed) {
            const scale = maxSpeed / speed;
            Matter.Body.setVelocity(bubble, { x: bubble.velocity.x * scale, y: bubble.velocity.y * scale });
        } else if (maxSpeed === 0) {
            Matter.Body.setVelocity(bubble, { x: 0, y: 0 });
        }
    });
});

// Appel de la fonction pour dessiner les joueurs
drawPlayers();

// Fonction pour sauvegarder l'état de l'application
function saveState() {
    const state = {
        bubbles: bubbles.map(bubble => ({
            position: bubble.position,
            velocity: bubble.velocity,
            restitution: bubble.restitution,
            friction: bubble.friction,
            fillStyle: bubble.render.fillStyle
        })),
        players
    };
    localStorage.setItem('bulloterieState', JSON.stringify(state));
    alert('État sauvegardé !');
}

// Fonction pour charger l'état de l'application
function loadState() {
    const state = JSON.parse(localStorage.getItem('bulloterieState'));
    if (state) {
        bubbles.forEach(bubble => World.remove(engine.world, bubble));
        bubbles = [];
        players = {};
        state.bubbles.forEach(bubbleData => {
            const bubble = createBubble(bubbleData.position);
            World.add(engine.world, bubble);
            bubbles.push(bubble);
        });
        players = state.players;
        updateDashboard();
        alert('État chargé !');
    } else {
        alert('Aucun état sauvegardé trouvé.');
    }
}

// Fonction pour animer le contour des bulles sélectionnées
function animateBubbleStroke(bubble) {
    let growing = true; // Indicateur pour savoir si le contour grandit ou rétrécit
    const animate = () => {
        if (selectedBubbles.includes(bubble)) {
            const currentWidth = parseFloat(bubble.render.lineWidth);
            const newWidth = growing ? currentWidth + 0.5 : currentWidth - 0.5;

            // Limiter l'épaisseur du contour
            if (newWidth >= bubble.properties.radius * 0.2 + 4) {
                growing = false; // Commencer à réduire
            } else if (newWidth <= bubble.properties.radius * 0.2 - 4) {
                growing = true; // Commencer à grandir
            }

            // Mettre à jour l'épaisseur du contour
            bubble.render.lineWidth = newWidth; 
            // Appliquer la couleur de remplissage comme couleur de contour avec transparence
            const fillColor = bubble.properties.fillStyle; // Couleur de remplissage
            const rgbaColor = fillColor.replace('rgb', 'rgba').replace(')', ', 0.7)'); // Convertir en rgba avec transparence
            bubble.render.strokeStyle = rgbaColor; // Utiliser la couleur de remplissage avec transparence
            drawBubbles(); // Redessiner les bulles pour appliquer les changements
            requestAnimationFrame(animate); // Continuer l'animation
        }
    };
    animate(); // Démarrer l'animation
}

