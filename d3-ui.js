function deactivateCentralGravity() {
    gravityEnabled = false; // Désactiver la gravité centrale
    console.log("Gravité centrale désactivée.");
}

function activateCentralGravity() {
    gravityEnabled = true; // Activer la gravité centrale
    console.log("Gravité centrale activée.");
}



// Écouter les événements de touche
document.addEventListener('keydown', function(event) {
    if (event.code === 'Space') {
        appState.spacePressed = true; // La touche espace est enfoncée
    }

    // Vérifier si la touche espace est maintenue
    if (appState.spacePressed) {
        if (event.key === 'g') {
            gravityEnabled ? deactivateCentralGravity() : activateCentralGravity();
        } else if (event.key === 's') {
            saveState();
        } else if (event.key === 'l') {
            loadState();
        } else if (event.key === 'r') {
            resetBubbles(); // Appeler une fonction pour réinitialiser les bulles
        }
    }
});

// Écouter l'événement de relâchement de la touche
document.addEventListener('keyup', function(event) {
    if (event.code === 'Space') {
        appState.spacePressed = false; // La touche espace est relâchée
    }
});

// Fonction utilitaire pour créer un bouton avec une icône
function createButton(parent, iconClass, onClick) {
    return d3.select(parent).append("button")
        .html(`<i class="${iconClass}"></i>`) // Utiliser une icône au lieu de texte
        .on("click", onClick);
}

// Fonction pour créer des curseurs
function createSlider(property, bubble) {
    const sliderContainer = d3.select(`#bubble-${bubble.id}`).append("div").attr("class", "slider-container");
    sliderContainer.append("label")
        .html(`<i class="fas fa-sliders-h"></i>`) // Icône pour le curseur
        .append("input")
        .attr("type", "range")
        .attr("min", 0.0001)
        .attr("max", 2)
        .attr("step", 0.0001)
        .attr("value", bubble.properties[property])
        .on("input", function() {
            const value = parseFloat(this.value);
            // Mettre à jour les propriétés physiques de la bulle
            Matter.Body.set(bubble, {
                [property]: value
            });
            bubble.properties[property] = value;
            drawBubbles();
        });
}

// Fonction pour mettre à jour le tableau de bord
function updateDashboard() {
    d3.select("#dashboard").selectAll("div.bubble-info").remove();
    bubbles.forEach((bubble, index) => {
        const playerList = players[bubble.id] || [];
        const playerNames = playerList.map(player => `${player.name} (Niveau: ${player.level})`).join(", ");
        
        // Créer la div pour la bulle
        const bubbleInfo = d3.select("#dashboard").append("div")
            .attr("class", "bubble-info" + (selectedBubbles.includes(bubble) ? " selected" : "")) // Ajouter la classe 'selected' si la bulle est sélectionnée
            .attr("id", `bubble-${bubble.id}`);

        // Champ de saisie pour renommer la bulle
        bubbleInfo.append("input")
            .attr("type", "text")
            .attr("placeholder", "Nom")
            .attr("value", bubble.name || `Bulle ${index + 1}`)
            .on("change", function() {
                const newName = this.value;
                bubble.name = newName;
                drawBubbles();
            });

        // Créer des curseurs pour les propriétés physiques
        createSlider("restitution", bubble);
        createSlider("friction", bubble);
        createSlider("density", bubble);
        createSlider("airFriction", bubble);
        createSlider("maxSpeed", bubble);

        // Sélecteur de couleur pour le fond
        bubbleInfo.append("label")
            .html(`<i class="fas fa-paint-brush"></i>`) // Icône pour le sélecteur de couleur
            .append("input")
            .attr("type", "color")
            .attr("value", bubble.render.fillStyle) // Initialiser avec la couleur de remplissage
            .on("input", function() {
                const selectedColor = this.value;
                bubble.render.fillStyle = selectedColor;
                bubble.properties.fillStyle = selectedColor;
                drawBubbles();
            });

        // Sélecteur de couleur pour le contour
        bubbleInfo.append("label")
            .html(`<i class="fas fa-border-all"></i>`) // Icône pour le contour
            .append("input")
            .attr("type", "color")
            .attr("value", bubble.render.strokeStyle) // Initialiser avec la couleur de contour
            .on("input", function() {
                const selectedColor = this.value;
                bubble.render.strokeStyle = selectedColor;
                bubble.properties.strokeStyle = selectedColor;
                drawBubbles();
            });

        // Sélecteur pour le type de contour
        bubbleInfo.append("label")
            .html(`<i class="fas fa-th"></i>`) // Icône pour le type de contour
            .append("select")
            .on("change", function() {
                const selectedType = this.value;
                bubble.properties.lineType = selectedType;
                drawBubbles();
            })
            .selectAll("option")
            .data(["normal", "épais", "pointillé", "dashed", "double"])
            .enter()
            .append("option")
            .text(d => d);
    });
}

// Fonction utilitaire pour gérer la sélection des bulles
function toggleBubbleSelection(bubble) {
    if (!selectedBubbles.includes(bubble)) {
        selectedBubbles.push(bubble);
        bubble.render.strokeStyle = bubble.properties.fillStyle; // Couleur de sélection égale à la couleur de remplissage
        bubble.render.lineWidth = 4; // Épaisseur de sélection

        // Appliquer la couleur de remplissage avec transparence pour le contour
        const fillColor = bubble.properties.fillStyle; // Couleur de remplissage
        const rgbaColor = fillColor.replace('rgb', 'rgba').replace(')', ', 0.7)'); // Convertir en rgba avec transparence
        bubble.render.strokeStyle = rgbaColor; // Utiliser la couleur de remplissage avec transparence

        animateBubbleStroke(bubble); // Démarrer l'animation de pulsation
    } else {
        selectedBubbles = selectedBubbles.filter(b => b !== bubble);
        // Restaurer les propriétés de contour d'origine
        bubble.render.strokeStyle = bubble.properties.strokeStyle || 'transparent'; // Couleur d'origine
        bubble.render.lineWidth = bubble.properties.lineWidth || 0; // Épaisseur d'origine
    }
    updateDashboard(); // Mettre à jour le tableau de bord après la sélection
    drawBubbles(); // Redessiner les bulles pour appliquer les changements
}

// Fonction pour dessiner les bulles avec des styles de contour
function drawBubbles() {
    const bubbleSelection = d3.select("#canvas").selectAll("circle").data(bubbles, d => d.id);
    
    // Créer les cercles pour les bulles
    const circles = bubbleSelection.enter()
        .append("circle")
        .attr("class", "bubble")
        .attr("cx", d => d.position.x)
        .attr("cy", d => d.position.y)
        .attr("r", d => d.properties.radius)
        .style("fill", d => d.properties.fillStyle)
        .merge(bubbleSelection)
        .attr("cx", d => d.position.x)
        .attr("cy", d => d.position.y)
        .attr("r", d => d.properties.radius)
        .style("fill", d => d.properties.fillStyle);

    // Appliquer un contour épais blanc légèrement transparent aux bulles sélectionnées
    circles.style("stroke", d => selectedBubbles.includes(d) ? "rgba(255, 255, 255, 0.7)" : "transparent") // Couleur du contour
           .style("stroke-width", d => selectedBubbles.includes(d) ? 4 : 0); // Épaisseur du contour

    // Créer le texte pour chaque bulle
    const textSelection = bubbleSelection.enter()
        .append("text")
        .attr("class", "bubble-text")
        .attr("x", d => d.position.x)
        .attr("y", d => d.position.y)
        .attr("text-anchor", "middle")
        .attr("dominant-baseline", "middle")
        .text(d => d.name || `Bulle ${d.id}`) // Afficher le nom de la bulle
        .style("fill", "#fff")
        .style("font-size", "12px");

    // Mettre à jour le texte existant
    textSelection.merge(bubbleSelection)
        .attr("x", d => d.position.x)
        .attr("y", d => d.position.y)
        .text(d => d.name || `Bulle ${d.id}`); // Mettre à jour le texte avec le nouveau nom

    bubbleSelection.exit().remove();
}

// Création des boutons et curseurs
createButton("#dashboard", "fas fa-plus", addBubble); // Icône pour ajouter une bulle
createButton("#dashboard", "fas fa-trash", function() { // Icône pour supprimer les bulles sélectionnées
    selectedBubbles.forEach(bubble => {
        World.remove(engine.world, bubble);
        bubbles = bubbles.filter(b => b !== bubble);
    });
    selectedBubbles = [];
    updateDashboard();
});

// Option pour activer/désactiver la gravité centrale
createButton("#dashboard", "fas fa-gravity", function() { // Icône pour la gravité centrale
    gravityEnabled ? deactivateCentralGravity() : activateCentralGravity();
});

// Boutons pour sauvegarder et charger l'état
["fas fa-save", "fas fa-download"].forEach((iconClass, index) => {
    createButton("#dashboard", iconClass, index === 0 ? saveState : loadState);
});

// Fonction pour réinitialiser les bulles (à définir selon vos besoins)
function resetBubbles() {
    console.log("Les bulles ont été réinitialisées.");
}

// Initialiser le tableau de bord
updateDashboard();
// Créer le tooltip
const tooltip = d3.select("body").append("div")
    .attr("class", "tooltip");

// Événements de survol de la souris
Matter.Events.on(mouseConstraint, 'mouseover', function(event) {
    const bubble = mouseConstraint.body;
    if (bubble && bubble.render) {
        // Afficher le tooltip avec le nom de la bulle
        tooltip.style("display", "block")
            .text(bubble.name || `Bulle ${bubble.id}`)
            .style("left", `${event.mouse.position.x + 10}px`) // Positionner le tooltip à droite de la souris
            .style("top", `${event.mouse.position.y + 10}px`); // Positionner le tooltip en dessous de la souris

        originalBubbleSize = bubble.properties.radius; // Stocker la taille d'origine
        const scaleFactor = 1.33; // 33% d'augmentation
        const newRadius = Math.min(originalBubbleSize * scaleFactor, MAX_RADIUS); // Limiter à MAX_RADIUS
        Matter.Body.scale(bubble, newRadius / originalBubbleSize, newRadius / originalBubbleSize);
        bubble.properties.radius = newRadius; // Mettre à jour le rayon
    }
});

// Événements de fin de survol de la souris
Matter.Events.on(mouseConstraint, 'mouseout', function(event) {
    const bubble = mouseConstraint.body;
    if (bubble && bubble.render) {
        // Masquer le tooltip
        tooltip.style("display", "none");

        Matter.Body.scale(bubble, originalBubbleSize / bubble.properties.radius, originalBubbleSize / bubble.properties.radius);
        bubble.properties.radius = originalBubbleSize; // Rétablir le rayon d'origine
    }
});

// Événements de mouvement de la souris
Matter.Events.on(mouseConstraint, 'mousemove', function(event) {
    const bubble = mouseConstraint.body;
    if (bubble) {
        // Mettre à jour la position du tooltip avec la position de la souris
        tooltip.style("left", `${event.mouse.position.x + 10}px`)
            .style("top", `${event.mouse.position.y + 10}px`);
    }
});

